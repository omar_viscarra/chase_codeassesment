package com.github.omievee.NYCSchools.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import com.github.omievee.NYCSchools.R
import com.github.omievee.NYCSchools.clicklistener.SchoolClickListener
import com.github.omievee.NYCSchools.model.SchoolData
import kotlinx.android.synthetic.main.list_item_view.view.*

//easily replace this view w/ another -  makes it more modular
class SchoolListView(context: Context, attributeSet: AttributeSet? = null) :
    ConstraintLayout(context, attributeSet), View.OnClickListener {

    var listener: SchoolClickListener? = null
    var school: SchoolData? = null

    init {
        View.inflate(context, R.layout.list_item_view, this)
        layoutParams = MarginLayoutParams(
            MarginLayoutParams.MATCH_PARENT,
            MarginLayoutParams.WRAP_CONTENT
        )
        main.setOnClickListener(this)
    }

    fun bind(school: SchoolData) {
        this.school = school
        schoolName.text = school.school_name
        academia.text = school.interest1
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.main -> listener?.onSchoolClicked(school ?: return)
        }
    }
}