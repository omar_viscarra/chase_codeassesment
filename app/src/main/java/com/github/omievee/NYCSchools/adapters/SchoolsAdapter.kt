package com.github.omievee.NYCSchools.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.github.omievee.NYCSchools.clicklistener.SchoolClickListener
import com.github.omievee.NYCSchools.model.SchoolData
import com.github.omievee.NYCSchools.views.SchoolListView

class SchoolsAdapter(
    private val list: List<SchoolData>,
    private val listener: SchoolClickListener
) :
    RecyclerView.Adapter<BaseViewHolder>() {
    /*
      vvv  Presentation data here vvv
         val data = Data()
          get ....
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return BaseViewHolder(SchoolListView(parent.context))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        (holder.itemView as SchoolListView).bind(list[position])
        holder.itemView.listener = listener
    }
}