package com.github.omievee.NYCSchools.network

import com.github.omievee.NYCSchools.BuildConfig
import com.github.omievee.NYCSchools.application.SchoolApp
import com.github.omievee.NYCSchools.utils.Constants
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
class StaticApiModule {

    @Provides
    @Singleton
    fun provideOkHttpClient(
        cache: Cache
    ): OkHttpClient.Builder {
        return OkHttpClient.Builder().apply {
            //Added for authentication... seems like the api didn't need it
            //  addInterceptor(AuthenticatedNetworkInterceptor())
            if (BuildConfig.DEBUG) {
                val logging = HttpLoggingInterceptor()
                logging.level = HttpLoggingInterceptor.Level.BODY
                this.addInterceptor(logging)
            }
            connectTimeout(1, TimeUnit.MINUTES)
            writeTimeout(1, TimeUnit.MINUTES)
            readTimeout(1, TimeUnit.MINUTES)
            dispatcher(Dispatcher().apply {
                maxRequests = 3
            })
            cache(cache)
        }
    }

    @Provides
    @Singleton
    fun provideApi(client: OkHttpClient.Builder): SchoolApi {
        return Retrofit.Builder()
            .baseUrl(Constants.MAIN_UPLOAD_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
            .client(client.build())
            .build()
            .create(SchoolApi::class.java)
    }

    @Provides
    @Singleton
    fun provideCache(application: SchoolApp): Cache {
        return Cache(application.cacheDir, 20 * 1024 * 1024)
    }

    @Provides
    @Singleton
    fun provideAuthenticatedRequestInterceptor(): AuthenticatedNetworkInterceptor {
        return AuthenticatedNetworkInterceptor()
    }

}