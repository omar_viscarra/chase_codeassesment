package com.github.omievee.NYCSchools.details

import com.github.omievee.NYCSchools.datamanager.SchoolDataManager
import com.github.omievee.NYCSchools.di.ActivityScope
import com.github.omievee.NYCSchools.di.FragmentScope
import com.github.omievee.NYCSchools.main.MainActivity
import com.github.omievee.NYCSchools.main.MainActivityPresenter
import dagger.Module
import dagger.Provides

@Module
class DetailsFragModule {
    @Provides
    @FragmentScope
    fun providePresenter(
        view: DetailsFragment,
        manager: SchoolDataManager
    ): DetailsFragPresenter =
        DetailsFragPresenter(view, manager)

}