package com.github.omievee.NYCSchools.di

import com.github.omievee.NYCSchools.application.SchoolApp
import com.github.omievee.NYCSchools.network.SchoolApi
import com.github.omievee.NYCSchools.network.StaticApiModule
import com.github.omievee.NYCSchools.datamanager.SchoolDataImpl
import com.github.omievee.NYCSchools.datamanager.SchoolDataManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module(includes = [StaticApiModule::class])
class AppModule {

    @Provides
    @Singleton
    fun provideDataManager(
        context: SchoolApp,
        api: SchoolApi
    ): SchoolDataManager {
        return SchoolDataImpl(
            context, api
        )
    }

}