package com.github.omievee.NYCSchools.network

import com.github.omievee.NYCSchools.model.SATData
import com.github.omievee.NYCSchools.model.SchoolData
import io.reactivex.Single
import retrofit2.http.GET

interface SchoolApi {


    @GET("/resource/s3k6-pzi2.json")
    fun onGetListOfSchools(): Single<Array<SchoolData>>

    @GET("/resource/f9bf-2cp4.json")
    fun onGetSATData(): Single<Array<SATData>>


}
