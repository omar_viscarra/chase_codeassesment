package com.github.omievee.NYCSchools.di

import com.github.omievee.NYCSchools.details.DetailsFragModule
import com.github.omievee.NYCSchools.details.DetailsFragment
import com.github.omievee.NYCSchools.main.MainActivity
import com.github.omievee.NYCSchools.main.MainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
interface AppBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    fun mainActivity(): MainActivity


    @FragmentScope
    @ContributesAndroidInjector(modules = [DetailsFragModule::class])
    fun details(): DetailsFragment
}