package com.github.omievee.NYCSchools.main

import com.github.omievee.NYCSchools.di.ActivityScope
import com.github.omievee.NYCSchools.datamanager.SchoolDataManager
import dagger.Module
import dagger.Provides


@Module
class MainActivityModule {

    @Provides
    @ActivityScope
    fun providePresenter(
        main: MainActivity,
        manager: SchoolDataManager
    ): MainActivityPresenter =
        MainActivityPresenter(main, manager)
}