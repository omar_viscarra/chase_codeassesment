package com.github.omievee.NYCSchools.clicklistener

import com.github.omievee.NYCSchools.model.SchoolData

interface SchoolClickListener {
    fun onSchoolClicked(school: SchoolData)
}