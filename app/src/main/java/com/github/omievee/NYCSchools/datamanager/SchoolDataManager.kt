package com.github.omievee.NYCSchools.datamanager

import com.github.omievee.NYCSchools.model.SATData
import com.github.omievee.NYCSchools.model.SchoolData
import io.reactivex.Single

interface SchoolDataManager {

    fun onGetArrayOfSchools(): Single<Array<SchoolData>>
    fun onGetSATData(): Single<Array<SATData>>
}