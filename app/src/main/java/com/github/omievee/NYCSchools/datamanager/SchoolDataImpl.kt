package com.github.omievee.NYCSchools.datamanager

import com.github.omievee.NYCSchools.application.SchoolApp
import com.github.omievee.NYCSchools.model.SATData
import com.github.omievee.NYCSchools.model.SchoolData
import com.github.omievee.NYCSchools.network.SchoolApi
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SchoolDataImpl(
    val context: SchoolApp,
    val api: SchoolApi
) : SchoolDataManager {

    override fun onGetArrayOfSchools(): Single<Array<SchoolData>> {
        return api
            .onGetListOfSchools()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun onGetSATData(): Single<Array<SATData>> {
        return api
            .onGetSATData()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}