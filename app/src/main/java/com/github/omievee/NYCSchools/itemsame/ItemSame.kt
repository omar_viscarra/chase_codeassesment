package com.github.omievee.NYCSchools.itemsame


interface ItemSame<T> {

    fun sameAs(same: T): Boolean
    fun contentsSameAs(same: T): Boolean
}