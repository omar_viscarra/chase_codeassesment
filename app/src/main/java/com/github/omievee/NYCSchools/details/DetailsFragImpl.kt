package com.github.omievee.NYCSchools.details

import com.github.omievee.NYCSchools.model.SATData

interface DetailsFragImpl {

    fun onHideProgress()
    fun onShowProgress()
    fun onSetSATDetails(data:SATData)
    fun onNoSATData()

}