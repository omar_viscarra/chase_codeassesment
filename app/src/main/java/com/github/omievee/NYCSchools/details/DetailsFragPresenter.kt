package com.github.omievee.NYCSchools.details

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.animation.Animation
import android.view.animation.Transformation
import android.widget.LinearLayout
import android.widget.Toast
import com.github.omievee.NYCSchools.R
import com.github.omievee.NYCSchools.datamanager.SchoolDataManager
import com.github.omievee.NYCSchools.model.SATData
import com.github.omievee.NYCSchools.model.SchoolData
import io.reactivex.disposables.Disposable

class DetailsFragPresenter(val view: DetailsFragment, val dataManager: SchoolDataManager) {

    fun expand(v: View) {
        v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        val targetHeight = v.measuredHeight
        v.layoutParams.height = 1
        v.visibility = View.VISIBLE
        val animate: Animation = object : Animation() {
            override fun applyTransformation(
                interpolatedTime: Float,
                t: Transformation
            ) {
                v.layoutParams.height =
                    if (interpolatedTime == 1f) LinearLayout.LayoutParams.WRAP_CONTENT else (targetHeight * interpolatedTime).toInt()
                v.requestLayout()
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }
        animate.duration = (targetHeight / v.context.resources
            .displayMetrics.density).toLong()
        v.startAnimation(animate)
    }

    fun collapse(v: View) {
        val initialHeight = v.measuredHeight
        val a: Animation = object : Animation() {
            override fun applyTransformation(
                interpolatedTime: Float,
                t: Transformation
            ) {
                if (interpolatedTime == 1f) {
                    v.visibility = View.GONE
                } else {
                    v.layoutParams.height =
                        initialHeight - (initialHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }
        a.duration = (initialHeight / v.context.resources
            .displayMetrics.density).toLong()
        v.startAnimation(a)
    }

    fun onDestroy() {
        satDisp?.dispose()
    }

    var satData: SATData? = null
    var satDisp: Disposable? = null
    fun onGetSATData(dbn: String?) {
        view.onShowProgress()
        satDisp?.dispose()
        satDisp = dataManager
            .onGetSATData()
            .map {
                satData = it.find {
                    it.dbn == dbn
                }
            }
            .subscribe({
                view.onHideProgress()
                if (satData != null) {
                    satData?.let {
                        view.onSetSATDetails(satData!!)
                    }
                } else view.onNoSATData()

            }, {
                it.printStackTrace()
            })
    }

    fun mapIntent(schoolObj: SchoolData?) {
        val uri = Uri.parse(
            "geo:" + schoolObj?.latitude + "," + schoolObj?.longitude + "?q=" + Uri.encode(
                schoolObj?.school_name
            )
        )
        try {
            val mapIntent = Intent(Intent.ACTION_VIEW, Uri.parse(uri.toString()))
            mapIntent.setPackage("com.google.android.apps.maps")
            view?.context?.startActivity(mapIntent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(view?.context, view?.context?.getString(R.string.maps_not_avail), Toast.LENGTH_SHORT)
                .show()
        } catch (x: Exception) {
            x.message
        }

    }

}