package com.github.omievee.NYCSchools.adapters

import android.view.View
import androidx.recyclerview.widget.RecyclerView

// base view holder to work w/ any view necessary --
class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)