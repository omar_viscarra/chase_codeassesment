package com.github.omievee.NYCSchools.main

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.omievee.NYCSchools.R
import com.github.omievee.NYCSchools.adapters.SchoolsAdapter
import com.github.omievee.NYCSchools.clicklistener.SchoolClickListener
import com.github.omievee.NYCSchools.details.DetailsFragment
import com.github.omievee.NYCSchools.model.SchoolData
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseApp(), MainActivityImpl {

    @Inject
    lateinit var presenter: MainActivityPresenter

    /* TODO:
     Given more time -
     I'd build a Room Database to store all data & only update every so often if data changed. Also for offline functionality.
     Have a splash screen (aesthetics)
     Create Search Bar at top...
     */

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        schoolRec.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        presenter.onGetData()
    }

    private val schoolClickListener = object : SchoolClickListener {
        override fun onSchoolClicked(school: SchoolData) {
            //Bottom sheet fragment felt right for displaying data while not abandoning the main list from view
            DetailsFragment
                .newInstance(schoolObject = school)
                .show(supportFragmentManager, "bottomSheet")
        }
    }

    override fun onShowProgress() {
        progressView.visibility = View.VISIBLE
    }

    override fun onHideProgress() {
        progressView.visibility = View.GONE
    }

    //If i had more time, I'd account for more complex data... Create  a PresentationView Class, nest the list in it.
    /*  see Details frag ---
    */

    override fun onUpdateAdapter(schoolList: List<SchoolData>) {
        val dataAdapter = SchoolsAdapter(schoolList, schoolClickListener)
        schoolRec.adapter = dataAdapter
    }


    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }
}
