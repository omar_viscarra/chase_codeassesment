package com.github.omievee.NYCSchools.main

import com.github.omievee.NYCSchools.model.SchoolData

interface MainActivityImpl {

    fun onShowProgress()
    fun onHideProgress()
    fun onUpdateAdapter(schoolList: List<SchoolData>)
}