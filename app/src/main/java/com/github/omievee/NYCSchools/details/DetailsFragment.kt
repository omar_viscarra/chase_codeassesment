package com.github.omievee.NYCSchools.details

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.github.omievee.NYCSchools.R
import com.github.omievee.NYCSchools.model.SATData
import com.github.omievee.NYCSchools.model.SchoolData
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.bottom_sheet_details.*
import javax.inject.Inject


private const val DETAILS_PARAM = "details"

class DetailsFragment : BottomSheetDialogFragment(), DetailsFragImpl, View.OnClickListener {

    @Inject
    lateinit var presenter: DetailsFragPresenter

    private var schoolObj: SchoolData? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            schoolObj = it.getParcelable(DETAILS_PARAM)
        }
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.bottom_sheet_details, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.onGetSATData(schoolObj?.dbn)
        details1.setOnClickListener(this)
        details2.setOnClickListener(this)
        locationView.setOnClickListener(this)
        onDisplayData()
    }

    private fun onDisplayData() {
        name.text = schoolObj?.school_name
        address.text = schoolObj?.primary_address_line_1
        address2.text = "${schoolObj?.city} ${schoolObj?.state_code}, ${schoolObj?.zip}"
        overView.text = schoolObj?.overview_paragraph
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    companion object {
        @JvmStatic
        fun newInstance(schoolObject: SchoolData) =
            DetailsFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(DETAILS_PARAM, schoolObject)
                }
            }
    }

    var isMissionExpanded: Boolean = false
    var isSATExpanded: Boolean = false
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.details1 -> checkMissionExpand()
            R.id.details2 -> checkSATExpanded()
            R.id.locationView -> presenter.mapIntent(schoolObj)

        }
    }


    private fun checkSATExpanded() {
        if (!isSATExpanded) {
            isSATExpanded = true
            presenter.expand(nestedExpand)
            nestedExpand.visibility = View.VISIBLE
        } else {
            isSATExpanded = false
            presenter.collapse(nestedExpand)
            nestedExpand.visibility = View.GONE
        }
    }

    private fun checkMissionExpand() {
        if (!isMissionExpanded) {
            isMissionExpanded = true
            presenter.expand(overView)
            overView.visibility = View.VISIBLE
        } else {
            isMissionExpanded = false
            presenter.collapse(overView)
            overView.visibility = View.GONE
        }
    }

    override fun onHideProgress() {
        progressView.visibility = View.GONE
    }

    override fun onShowProgress() {
        progressView.visibility = View.VISIBLE
    }

    override fun onSetSATDetails(details: SATData) {
        writing.text = "${getString(R.string.sat_writing_avg)} ${details?.sat_writing_avg_score}"
        critReading.text =
            "${getString(R.string.sat_crit_avg)}  ${details?.sat_critical_reading_avg_score}"
        math.text = "${getString(R.string.sat_math_avg)}  ${details?.sat_math_avg_score}"

    }

    override fun onNoSATData() {
        details2.setOnClickListener(null)
        details2.alpha = .5F
        noData.visibility = View.VISIBLE
    }
}

/* TODO: if allowed time
    Create a Recyclerview for details (was started but back tracked)
    allowing for more modular view & easily add data.  Below code is example of started
 */


//class DetailsViewData(
//    val data: List<DetailsPresentation>,
//    val diffResult: DiffUtil.DiffResult
//)
//
//enum class Details {
//    SCHOOL_MISSION.
//
//}
//
//data class DetailsPresentation(
//    val type: Details,
//    val title: String
//) : ItemSame<DetailsPresentation> {
//
//    override fun sameAs(same: DetailsPresentation): Boolean {
//        return equals(same)
//    }
//
//    override fun contentsSameAs(same: DetailsPresentation): Boolean {
//        return hashCode() == same.hashCode()
//    }
//}
