package com.github.omievee.NYCSchools.main

import com.github.omievee.NYCSchools.datamanager.SchoolDataManager
import io.reactivex.disposables.Disposable

class MainActivityPresenter(val view: MainActivity, private val manager: SchoolDataManager) {


    var dataDisp: Disposable? = null
    fun onGetData() {
        view.onShowProgress()
        dataDisp?.dispose()
        dataDisp = manager
            .onGetArrayOfSchools()
            .subscribe({
                it.sortBy {
                    it.school_name
                }
                view.onUpdateAdapter(it.toList())
                view.onHideProgress()
            }, {
                view.onHideProgress()
                println("From PResenter: ${it.printStackTrace()}")
            })

    }

    fun onDestroy() {
        dataDisp?.dispose()
    }
}